package com.poby.mygdx.game;


public class World {
    private Pacman pacman;
    private PacmanGame pacmanGame;
    private Maze maze;
    private int score;

    World(PacmanGame pacmanGame) {
        this.maze = new Maze();
        this.pacman = new Pacman(60,60,this);
        this.pacmanGame = pacmanGame;
        this.score = 0;
    }

    Pacman getPacman() {
        return this.pacman;
    }

    Maze getMaze() {
        return this.maze;
    }

    public void update(float delta) {
        this.pacman.update();
    }
    public int getScore() {
        return this.score;
    }

    public void increaseScore() {
        this.score += 1;
    }
}