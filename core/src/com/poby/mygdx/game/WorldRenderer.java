package com.poby.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class WorldRenderer {
    public static final int BLOCK_SIZE = 40;
    private PacmanGame pacmanGame;
    private Texture pacmanImg;
    private World world;
    public SpriteBatch batch;
    private Pacman pacman;
    private MazeRenderer mazeRenderer;
    private BitmapFont font;


    public WorldRenderer(PacmanGame pacmanGame, World world) {

        this.pacmanGame = pacmanGame;
        this.batch = pacmanGame.batch;
        this.world = world;
        this.pacman = world.getPacman();
        this.pacmanImg = new Texture("pacman.png");
        this.mazeRenderer = new MazeRenderer(pacmanGame.batch, world.getMaze());
        this.font = new BitmapFont();

    }

    public void render(float delta) {
        this.mazeRenderer.render();
        SpriteBatch batch = this.pacmanGame.batch;
        Vector2 pos = this.world.getPacman().getPosition();
        batch.begin();
        batch.draw(this.pacmanImg, pos.x - BLOCK_SIZE/2,
                PacmanGame.HEIGHT - pos.y - BLOCK_SIZE/2);
        this.font.draw(batch, "score"+""+ this.world.getScore(), 700, 60);
        batch.end();
    }

}
