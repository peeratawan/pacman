package com.poby.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MazeRenderer {
    private Maze maze;
    private SpriteBatch batch;
    private Texture wallImage;
    private Texture dotImage;

    public MazeRenderer(SpriteBatch batch, Maze maze) {
        this.maze = maze;
        this.batch = batch;
        this.wallImage = new Texture("wall.png");
        this.dotImage = new Texture("dot.png");
    }

    public void render() {
        this.batch.begin();
        for(int r = 0; r < this.maze.getHeight(); r++) {
            for(int c = 0; c < this.maze.getWidth(); c++) {
                int x = c * WorldRenderer.BLOCK_SIZE;
                int y = PacmanGame.HEIGHT -
                        (r * WorldRenderer.BLOCK_SIZE) - WorldRenderer.BLOCK_SIZE;

                if(this.maze.hasWallAt(r, c)) {
                    this.batch.draw(this.wallImage, x, y);
                } else if(this.maze.hasDotAt(r, c)) {
                    this.batch.draw(this.dotImage, x, y);
                }
            }
        }
        this.batch.end();
    }
}
