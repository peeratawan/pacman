package com.poby.mygdx.game;


public class Maze {
    private String[] MAP = new String [] {
            "####################",
            "#..................#",
            "#.###.###..###.###.#",
            "#.#...#......#...#.#",
            "#.#.###.####.###.#.#",
            "#.#.#..........#.#.#",
            "#.....###..###.....#",
            "#.#.#..........#.#.#",
            "#.#.###.####.###.#.#",
            "#.#...#......#...#.#",
            "#.###.###..###.###.#",
            "#..................#",
            "####################"
    };

    private int height;
    private int width;
    private boolean [][] hasDots;

    public Maze() {
        this.height = this.MAP.length;
        this.width = this.MAP[0].length();
        initDotData();
    }

    private void initDotData() {
        this.hasDots = new boolean[this.height][this.width];
        for(int r = 0; r < this.height; r++) {
            for(int c = 0; c < this.width; c++) {
                this.hasDots[r][c] = this.MAP[r].charAt(c) == '.';
            }
        }
    }


    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public boolean hasWallAt(int r, int c) {
        return this.MAP[r].charAt(c) == '#';
    }

    public boolean hasDotAt(int r, int c) {
        return this.hasDots[r][c];
    }

    public void removeDotAt(int r, int c) {
        this.hasDots[r][c] = false;
    }
}
