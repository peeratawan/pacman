package com.poby.mygdx.game;

import com.badlogic.gdx.math.Vector2;

public class Pacman {

    private Vector2 position;
    private int currentDirection;
    private int nextDirection;
    private World world;

    public static final int DIRECTION_UP = 1;
    public static final int DIRECTION_RIGHT = 2;
    public static final int DIRECTION_DOWN = 3;
    public static final int DIRECTION_LEFT = 4;
    public static final int DIRECTION_STILL = 0;

    public static final int SPEED = 5;

    public Pacman(int x, int y,World world) {
        this.position = new Vector2(x,y);
        this.currentDirection = DIRECTION_STILL;
        this.nextDirection = DIRECTION_STILL;
        this.world = world;
    }


    public Vector2 getPosition() {
        return this.position;
    }

    private static final int [][] DIR_OFFSETS = new int [][] {
        {0,0},
        {0,-1},
        {1,0},
        {0,1},
        {-1,0}
    };

    public void setNextDirection(int dir) {
        this.nextDirection = dir;
    }

    public void update() {
        Maze maze = this.world.getMaze();
        if(isAtCenter()) {
            if(canMoveInDirection(this.nextDirection)) {
                this.currentDirection = this.nextDirection;
            } else {
                this.currentDirection = DIRECTION_STILL;
            }
            int r = getRow();
            int c = getColumn();
            if(maze.hasDotAt(r,c)){
                maze.removeDotAt(r, c);
                this.world.increaseScore();
            }
        }
        this.position.x += SPEED * DIR_OFFSETS[this.currentDirection][0];
        this.position.y += SPEED * DIR_OFFSETS[this.currentDirection][1];
    }

    public boolean isAtCenter() {
        int blockSize = WorldRenderer.BLOCK_SIZE;

        return ((((int)this.position.x - blockSize/2) % blockSize) == 0) &&
                ((((int)this.position.y - blockSize/2) % blockSize) == 0);
    }

    private boolean canMoveInDirection(int dir) {
        Maze maze = this.world.getMaze();
        int newRow = getRow()+ DIR_OFFSETS[dir][1];
        int newCol = getColumn()+ DIR_OFFSETS[dir][0];

        if(maze.hasWallAt(newRow,newCol)){
            return false;
        }
        return true;
    }

    private int getRow() {
        return (((int)(this.position.y - WorldRenderer.BLOCK_SIZE /2) / WorldRenderer.BLOCK_SIZE));
    }

    private int getColumn() {
        return (((int)(this.position.x - WorldRenderer.BLOCK_SIZE /2) / WorldRenderer.BLOCK_SIZE));
    }


}