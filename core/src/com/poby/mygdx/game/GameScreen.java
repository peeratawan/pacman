package com.poby.mygdx.game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class GameScreen extends ScreenAdapter {

    private PacmanGame pacmanGame;
    private Texture pacmanImg;
    private Pacman pacman;
    World world;
    private WorldRenderer worldRenderer;

    public GameScreen(PacmanGame pacmanGame) {
        this.world = new World(pacmanGame);
        this.worldRenderer = new WorldRenderer(pacmanGame,this.world);
        this.pacman = this.world.getPacman();
    }
    private void update(float delta) {
        updatePacmanDirection();
        this.world.update(delta);
    }

    private void updatePacmanDirection() {
        boolean isKeyPressed = false;
        if(Gdx.input.isKeyPressed(Keys.LEFT)) {
            this.pacman.setNextDirection(Pacman.DIRECTION_LEFT);
            isKeyPressed = true;
        }
        if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
            this.pacman.setNextDirection(Pacman.DIRECTION_RIGHT);
            isKeyPressed = true;
        }
        if(Gdx.input.isKeyPressed(Keys.UP)) {
            this.pacman.setNextDirection(Pacman.DIRECTION_UP);
            isKeyPressed = true;
        }
        if(Gdx.input.isKeyPressed(Keys.DOWN)) {
            this.pacman.setNextDirection(Pacman.DIRECTION_DOWN);
            isKeyPressed = true;
        }
        if(!isKeyPressed){
            this.pacman.setNextDirection(Pacman.DIRECTION_STILL);
        }
    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        this.worldRenderer.render(delta);
    }
}

